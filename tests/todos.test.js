const app = require("../app");
const request = require("supertest");
const { sequelize } = require("../models");
const { queryInterface } = sequelize;

beforeAll((done) => {
    queryInterface.bulkInsert("Todos",
        [
            {
                id: 1,
                title: "Lorem Ipsum",
                description: "Lorem ipsum dolor sit amet consectetur adipisicing elit.",
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                id: 2,
                title: "Lorem Ipsum 2",
                description: "2 Lorem ipsum dolor sit amet consectetur adipisicing elit.",
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                id: 3,
                title: "Lorem Ipsum 3",
                description: "3 Lorem ipsum dolor sit amet consectetur adipisicing elit.",
                createdAt: new Date(),
                updatedAt: new Date()
            }
        ]
        , {})
        .then(_ => {
            done()
        })
        .catch(err => {
            console.log(err);
            done(err);
        })
})


afterAll((done) => {
    queryInterface.bulkDelete("Todos", null, {})
        .then(_ => {
            done();
        })
        .catch(err => {
            console.log(err);
            done(err);
        })
})

describe('Testing Todos', () => {
    it("Findall Todos", (done) => {
        request(app)
            .get("/todos")
            .expect('Content-Type', /json/)
            .expect(200)
            .then(response => {
                const { body, status } = response;
                expect(status).toEqual(200);
                expect(body.length).toEqual(3);
                const items = body[0];

                expect(items.title).toBe("Lorem Ipsum");
                expect(items.description).toBe("Lorem ipsum dolor sit amet consectetur adipisicing elit.")
                done(); // Call the done() function to indicate the test is complete
            })
            .catch(err => {
                done(err); // Call done() with the error if any error occurs
            });
    }, 10000); // Set a timeout of 10000 milliseconds (10 seconds)
});

it("Find detail Todos", (done) => {

    request(app)
        .get("/todos/1")
        .expect('Content-Type', /json/)
        .expect(200)
        .then(response => {
            const { body, status } = response;
            expect(status).toEqual(200);
            done()
        })
        .catch(err => {
            console.log(err);
            done(err);
        })
})

it("Create todos", (done) => {

    request(app)
        .post("/todos")
        .send({
            title: "Lorem Ipsum 4",
            description: "4 Lorem ipsum dolor sit amet consectetur adipisicing elit."
        })
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(201)
        .then(response => {
            const { body, status } = response;

            expect(body.message).toEqual("Todos created")
            done();
        })
        .catch(err => {
            console.log(err);
            done(err);
        })
})

it("Update Todos", (done) => {

    request(app)
        .put("/todos/1")
        .send({
            title: "Lorem Ipsum",
            description: "Lorem ipsum dolor sit amet consectetur adipisicing elit."
        })
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .then(response => {
            const { body, status } = response;

            expect(body.message).toEqual("Todos updated")
            done();
        })
        .catch(err => {
            console.log(err);
            done(err);
        })
})

it("Delete Todos", (done) => {

    request(app)
        .delete("/todos/1")
        .expect('Content-Type', /json/)
        .expect(200)
        .then(response => {
            const { body, status } = response;
            expect(body.message).toEqual("Todos deleted")
            done();
        })
})