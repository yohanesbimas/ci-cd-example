'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert("Todos",
      [
        {
          title: "Lorem Ipsum",
          description: "Lorem ipsum dolor sit amet consectetur adipisicing elit.",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          title: "Lorem Ipsum 2",
          description: "2 Lorem ipsum dolor sit amet consectetur adipisicing elit.",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          title: "Lorem Ipsum 3",
          description: "3 Lorem ipsum dolor sit amet consectetur adipisicing elit.",
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ], {})
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete("Todos", null, {})
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};