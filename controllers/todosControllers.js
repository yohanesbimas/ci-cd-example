const { Todos } = require("../models");

class TodosController {
    static findAll = async (req, res, next) => {
        try {
            const todos = await Todos.findAll();
            res.status(200).json(todos)
        } catch (err) {
            next(err)
        }

    }

    static findOne = async (req, res, next) => {
        try {
            const { id } = req.params;
            const todos = await Todos.findOne({
                where: {
                    id
                }
            });
            if (!todos) {
                throw { name: "ErrorNotFound" }
            }

            res.status(200).json(todos);
        } catch (err) {
            next(err);
        }
    }


    static create = async (req, res, next) => {
        try {
            const { title, description } = req.body;

            await Todos.create({
                title,
                description
            })

            res.status(201).json({ message: "Todos created" });
        } catch (err) {
            next(err);
        }
    }

    static update = async (req, res, next) => {
        try {
            const { id } = req.params;
            const { title, description } = req.body;

            // Todos find
            const foundTodos = await Todos.findOne({
                where: {
                    id
                }
            })

            if (!foundTodos) {
                throw { name: "ErrorNotFound" }
            }

            await foundTodos.update({
                title: title || foundTodos.title,
                description: description || foundTodos.description,
            })

            // foundProduct data lama

            await foundTodos.save();

            // foundTodos data terupdate

            res.status(200).json({ message: "Todos updated" })
        } catch (err) {
            next(err);
        }
    }

    static destroy = async (req, res, next) => {
        try {
            const { id } = req.params;

            const foundTodos = await Todos.findOne({
                where: {
                    id
                }
            })

            if (!foundTodos) {
                throw { name: "ErrorNotFound" }
            }

            await foundTodos.destroy();

            res.status(200).json({ message: "Todos deleted" })
        } catch (err) {
            next(err);
        }
    }


}

module.exports = TodosController;