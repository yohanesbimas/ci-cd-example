const express = require("express");
const router = express.Router();
const TodosController = require("../controllers/todosControllers.js");

router.get("/", TodosController.findAll);
router.get("/:id", TodosController.findOne);
router.post("/", TodosController.create);
router.put("/:id", TodosController.update);
router.delete("/:id", TodosController.destroy);

module.exports = router;